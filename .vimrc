" Use Vim settings, Placed as first thing to prevent issues
set nocompatible

" =============== Pathogen Initialization ===============
" This loads pathogen which manages all vim plugins
" Plugins should go at ~/.vim/bundle/<plugin-Name>/

  runtime bundle/tpope-vim-pathogen/autoload/pathogen.vim
  call pathogen#infect()
  call pathogen#helptags()

" ================ General Config ====================

set nu                          "Enables Line Numbers
set history=1500                "Sets command line history to 1500
set showcmd                     "Show incomplete cmds down the bottom
set gcr=a:blinkon0              "Disables cursor blink
set autoread                    "Reload the file if it is changed since being opened
set backspace=indent,eol,start  "Allow backspace
set visualbell                  "No sounds
set showmode                    "Show current mode down the bottom
set wildmenu                    "Enables new buffers as tabs
set hidden		            "Buffers exist in background
syntax on			            "turn on syntax highlighting
set t_vb=                       "Removes annoying screen flash and EOF
colorscheme elflord             "Sets the default colorscheme to blue
"Allows elevating status to root in order to save a readonly file
command W execute "w !sudo dd of=%"  

" ===================== Spelling ====================

" Sets F7 to toggle spell checker
map <silent> <f7> :set nospell!<CR>:set nospell?<cr>

" ================ Search Settings  =================

set incsearch        "Find the next match as we type the search
set hlsearch         "Highlight searches by default
set viminfo='100,f1  "Save up to 100 marks, enable capital marks

" ================ Arduino Settings  ================

map <silent> <leader>au :!ino upload<cr>
map <silent> <leader>ac :!ino build<cr>
map <silent> <leader>as :!ino serial<cr>


" ================ Arduino Settings  ================

map <silent> <leader>m :make<cr>
" ================ Special Syntax  ==================

au BufRead,BufNewFile *.pde set filetype=arduino "Enable syntax highlighting for arduino files
au BufRead,BufNewFile *.ino set filetype=arduino "Enable syntax highlighting for arduino files


" ================ sparkup FTP fixes  ==================
augroup sparkup_types
     autocmd!
     autocmd FileType php,html,eruby runtime! ftplugin/html/sparkup.vim
augroup END

"***************** Below is from the interwebs ***************************

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowb

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.

silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile

" ================ Indentation ======================

set autoindent
set smartindent
set smarttab
set shiftwidth=5
set softtabstop=5
set tabstop=5
set expandtab

filetype plugin on
filetype indent on

" Display tabs and trailing spaces visually
set list listchars=tab:\ \ ,trail:·

set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points

" ================ License Auto Add =================

map lic :0r ~/licenses/

" ================ Folds ============================

set foldmethod=indent   "fold based on indent
set foldnestmax=3       "deepest fold is 3 levels
set nofoldenable        "dont fold by default

" ================ Completion =======================

set wildmode=list:longest
set wildmenu                "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif

"

" ================ Scrolling ========================

set scrolloff=8         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1
